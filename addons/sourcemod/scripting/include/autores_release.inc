#if defined _autores_release_included
  #endinput
#endif
#define _autores_release_included

/*********************************************************
 * Get PlayerKiller Status
 *
 * @param client		The client to reset the data
 * @noreturn
 *********************************************************/
native bool Autores_Release_GetStatus();